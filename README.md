# Matrix Unreal

Matrix Unreal is a [Matrix](https://matrix.org/) client library built to work natively with the Unreal Game Engine. 
All code is self contained with no external dependencies other than the Unreal Game Engine itself.

## Demo Project
->> [Click Here](https://gitlab.com/devon.hudson/matrix-unreal-demo) <<- For an example project using Matrix Unreal to create a simple Matrix chat client.
