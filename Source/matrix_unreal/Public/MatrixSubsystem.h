// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "MatrixSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class MATRIX_UNREAL_API UMatrixSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
  UFUNCTION(BlueprintCallable, Category = "MatrixAPI")
  void StartTickTimer(float DeltaSeconds = 10.0f);

private:
  FTimerHandle TickTimerHandle;

  UFUNCTION()
  void Tick();
};
