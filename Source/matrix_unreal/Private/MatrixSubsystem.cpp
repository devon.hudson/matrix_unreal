// Fill out your copyright notice in the Description page of Project Settings.


#include "MatrixSubsystem.h"
#include "Internationalization/Text.h"
#include "Logging/LogMacros.h"


void UMatrixSubsystem::StartTickTimer(float DeltaSeconds)
{
  UE_LOG(LogTemp, Warning, TEXT("Tick time"));

  GetWorld()->GetTimerManager().SetTimer(TickTimerHandle, this, &UMatrixSubsystem::Tick, DeltaSeconds, true);
}

void UMatrixSubsystem::Tick()
{
  UE_LOG(LogTemp, Warning, TEXT("Tick!"));
}
